package week04;

import java.net.*;
import java.io.*;
import java.util.*;

public class HTTP {

    public static String getContent(String string) {

        String content="";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader r = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line= r.readLine()) != null) {
                sBuilder.append(line + "\n");
            }
            content = sBuilder.toString();

        } catch (IOException e) {
            System.err.println(e.toString());
        }
        catch (Exception e) {
            System.err.println(e.toString());
        }

        return content;
    }

    public static Map getHeader(String string) {
        Map hashmap = null;

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            hashmap = http.getHeaderFields();

        } catch(Exception e) {
            System.err.println(e.toString());
        }

        return hashmap;

    }

    public static <JSON> void main(String[] args) {
        System.out.println(HTTP.getContent("https://bitbucket.org/mizuki714/test/src/master/test"));

        Map<Integer, List<String>> m = HTTP.getHeader("https://bitbucket.org/mizuki714/test/src/master/test");


        for (Map.Entry<Integer,List<String>> entry : m.entrySet()) {
            try {
                System.out.println("Key= " + entry.toString() +   // The reason why getValue produced an error was because
                        entry.getValue()); // it contains a list of strings within each value.
                // Changing the type from <String> to List<String> you
                // can at least see the first entry in each value
            } catch(Exception e) {
                System.err.println(e.toString());
            }

            JSON info = (JSON) new Json();
            ((Json) info).setbandName("Lady GaGa");
            ((Json) info).setSongTitle("Poker Face");
            ((Json) info).setAlbum("Monster");
            ((Json) info).setYear(2006);

            JSON mJson = (JSON) httpJSON.httpToJSON(info);
            System.out.println(mJson);

            String info2 = httpJSON.httpToJSON(mJson);
            System.out.println(info2);
        }
    }
}

