package week04;

public class Json {
    private String bandName;
    private String Album;
    private String songTitle;
    private int year;

    //get band name
    public String getBandName(){
        return bandName;
    }
    //set band name
    public void setbandName(String bandName){
        this.bandName = bandName;
    }
    //get album name
    public String getAlbum(){
        return Album;
    }
    //set album name
    public void setAlbum(String Album){
        this.Album = Album;
    }
    //get song title
    public String getSongTitle(){
        return songTitle;
    }
    //set song title
    public void setSongTitle(String songTitle){
        this.songTitle = songTitle;
    }
    //get release year
    public int getYear(){
        return year;
    }
    //set release year
    public void setYear(int year){
        this.year = year;
    }
    public String toString(){
        return "Band Name:" + bandName + "\nAlbum: " + Album +"\nSong Title: " + songTitle + "\n Year: " + year;
    }

}