/*

@Julie Huntsman

*/
import java.util.InputMismatchException;
import java.util.Scanner;
 class week03 {
     public static void main(String[] args) throws Exception {

         double n1, n2,/* sum, product, diff,*/ quot;
         boolean again = true;
         String tryAgain;
         boolean num = false;

         do {
             System.out.println("Enter two Numbers 1-9 to add together");
             Scanner input = new Scanner(System.in);
             System.out.println("Enter the first number:");
             n1 = input.nextInt();
             Scanner input2 = new Scanner(System.in);
             System.out.println("Enter the second number:");
             n2 = input2.nextInt();
             Scanner input3 = new Scanner(System.in);
             //  sum = n1 + n2;  (n1 <= 0 || n1 >= 10) || (n2 <= 0 || n2 >= 10)
             //  product = n1 * n2;
             //  diff = n1 -n2;
             quot = n1 / n2;
             try {
                 if (n2 <= 0)
                     throw new ArithmeticException("Input is invalid");
                 System.out.println("The Quotent of " + n1 + " and " + n2 + " equals " + quot);
             } catch (ArithmeticException e) {
                 System.out.println("You Entered 0 as the denominator,\nplease choose a different number.");
                //1

             } finally {
                 System.out.println("Would you like to try again?\n Press Y or N");
                 tryAgain = input3.nextLine();
                 try {
                     if (tryAgain.equals("Y") || tryAgain.equals("y")) {
                         again = true;
                     } else if (tryAgain.equals("N") || tryAgain.equals("n")) {
                         again = false;
                         System.out.println("Goodbye");
                         System.exit(0);
                     } else if (!(tryAgain.equals("N") || tryAgain.equals("n"))
                             || tryAgain.equals("Y") || tryAgain.equals("y")) {
                         throw new InputMismatchException("Input is invalid");
                     }
                 } catch (InputMismatchException e) {
                     System.out.println("You did not enter Y or N. Try Again");

                 }
             }
         } while (again = true);
     }
 }



