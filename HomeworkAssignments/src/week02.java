//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

public class week02 {

    public static void main(String[] args) {
        System.out.println("\n List \n");
        ArrayList<String> listGroceries = new ArrayList();
        listGroceries.add("Bread");
        listGroceries.add("Milk");
        listGroceries.add("Eggs");
        listGroceries.add("Apples");
        listGroceries.add("Cereal");
        listGroceries.add("Broccoli");
        listGroceries.add("Bread");
        listGroceries.add("Turkey");
        System.out.println(listGroceries);
        System.out.println(" \n Set \n");
        Set setGroceries = new HashSet();
        setGroceries.add("Bread");
        setGroceries.add("Milk");
        setGroceries.add("Eggs");
        setGroceries.add("Apples");
        setGroceries.add("Cereal");
        setGroceries.add("Broccoli");
        setGroceries.add("Bread");
        setGroceries.add("Turkey");
        System.out.println(setGroceries);
        System.out.println(" \n Map \n");
        Map mapGroceries = new HashMap();
        mapGroceries.put(1, "Bread");
        mapGroceries.put(2, "Milk");
        mapGroceries.put(3, "Eggs");
        mapGroceries.put(4, "Apples");
        mapGroceries.put(5, "Cereal");
        mapGroceries.put(6, "Broccoli");
        mapGroceries.put(7, "Bread");
        mapGroceries.put(5, "Turkey");

        for(int i = 1; i < 8; ++i) {
            String groceryMap = (String)mapGroceries.get(i);
            System.out.println(groceryMap);
        }

        System.out.println(" \n Queue \n");
        Queue queueGroceries = new PriorityQueue();
        queueGroceries.add("Bread");
        queueGroceries.add("Milk");
        queueGroceries.add("Eggs");
        queueGroceries.add("Apples");
        queueGroceries.add("Cereal");
        queueGroceries.add("Broccoli");
        queueGroceries.add("Bread");
        queueGroceries.add("Turkey");
        System.out.println(queueGroceries);
        System.out.println(" \n Tree Set \n");
        Set treeSetGroceries = new TreeSet();
        treeSetGroceries.add("Bread");
        treeSetGroceries.add("Milk");
        treeSetGroceries.add("Eggs");
        treeSetGroceries.add("Apples");
        treeSetGroceries.add("Cereal");
        treeSetGroceries.add("Broccoli");
        treeSetGroceries.add("Bread");
        treeSetGroceries.add("Turkey");
        System.out.println(treeSetGroceries);
    }
}
