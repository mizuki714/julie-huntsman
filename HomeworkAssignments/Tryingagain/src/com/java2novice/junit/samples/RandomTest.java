package com.java2novice.junit.samples;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void getEmpNameWithHighestSalary() {

        String expectedName = "Jerry";
        assertEquals(expectedName, Random.getEmpNameWithHighestSalary());
    }

    @Test
    void getHighestPaidEmployee() {
        Random expectedEmpObj = new Random(100, "Jerry", 35000);
        assertEquals(expectedEmpObj, Random.getHighestPaidEmployee());
    }

    @Test
    void testHashCode() {
        Random empId = new Random("100");  // equals and hashCode check name field value
    }

    @Test
    void getName() {
        Random expectedName = new Random("Jerry");
        assertNotNull(expectedName);
    }

    @Test
    void setName() {
        Random expectedName = new Random("Jerry");
        assertNotNull(expectedName);
    }

    @Test
    void getEmpId() {
        Random expectedEmpObj = new Random(11, "Sue", 5000);
        assertNotSame(expectedEmpObj, Random.getHighestPaidEmployee());
    }
    
    @Test
    void testGetEmpId() {
        Random expectedEmpObj = new Random(10, "Gary", 3000);
        expectedEmpObj.getEmpId(10);
    }

    @Test
    void setEmpId() {
        Random expectedEmpObj = new Random(20, "Jim", 3100);
        expectedEmpObj.setEmpId(20);
    }

    @Test
    void getSalary() {
        Random expectedEmpObj = new Random(11, "Sue", 5000);
        expectedEmpObj.getSalary( 5000);
    }

    @Test
    void setSalary() {
        Random expectedEmpObj = new Random(11, "Sue", 5000);
        expectedEmpObj.setSalary(5000);
    }
}
