//Not my code


package com.java2novice.junit.samples;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Random{

    public static String name;
    public static Object Empid;
    //private String name;
    private int empId;
    private int salary;

    public Random(int id, String name, int sal){
        this.empId = id;
        this.name = name;
        this.salary = sal;
    }

    public Random(String s) {
    }

    public boolean equals(Object obj){
        Random emp = (Random) obj;
        boolean status = false;
        if(this.name.equalsIgnoreCase(emp.name)
                && this.empId == emp.empId
                && this.salary == emp.salary){
            status = true;
        }
        return status;
    }

    public static String getEmpNameWithHighestSalary(){
        /**
         * logic to get Highest paid employee
         */
        return "Jerry";
    }

    public static Random getHighestPaidEmployee(){
        /**
         * hiding logic to get highest paid employee
         */
        return new Random(100, "Jerry", 35000);
    }

    public int hashCode(){
        return this.empId;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getEmpId(int i) {
        return empId;
    }
    public void setEmpId(int empId) {
        this.empId = empId;
    }
    public int getSalary(int i) {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
}


